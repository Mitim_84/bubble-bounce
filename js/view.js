// this contains all the game rendering code

var imageLoader = new ImageLoader();


var bubbleGraphics = new Image();

function imagesLoaded(){
	// store this for convenience (shorter)
	bubbleGraphics = imageLoader.getImage(SPRITE_GRAPHICS_IMAGE);
	startPreGame();
}

// queue up the images to be loaded, only the last one needs a callback to start the game
imageLoader.queueAsset(SPRITE_GRAPHICS_IMAGE);
imageLoader.queueAsset(PRE_GAME_IMAGE);
imageLoader.queueAsset(GAME_OVER_IMAGE);
imageLoader.queueAsset(BACKGROUND_IMAGE, imagesLoaded);




// this is a set size in the sprite sheet (related to graphics which is why it is put here)
var BUBBLE_PIXEL_SIZE = 105;
var bulletGraphicOffset = BULLET_SIZE / 2;
var enemyGraphicOffset = ENEMY_SIZE / 2;

// UV coords for graphics
var blueBubbleUVCoords = new Rectangle(0, 0, 105, 105);
var redBubbleUVCoords = new Rectangle(105, 0, 105, 105);
var orangeBubbleUVCoords = new Rectangle(210, 0, 105, 105);
var yellowBubbleUVCoords = new Rectangle(315, 0, 105, 105);
var greenBubbleUVCoords = new Rectangle(420, 0, 105, 105);
var wallUVCoords = new Rectangle(0, 105, 500, 40);

// this is the uv coord for the number "0" graphic
// for each number use this rectangle and offset by the width * number up to "9"
var numberUVCoords = new Rectangle(0, 145, 40, 90);

// UV coords for the mute/un-mute button
var soundOnIcon = new Rectangle(410, 145, 40, 40);
var soundOffIcon = new Rectangle(455, 145, 40, 40);



var guyUVCoords = new Rectangle(392.8, 340.3, 59.5, 68.3);
var dinoUVCoords = new Rectangle(290.6, 340.3, 68.5, 68.3);

// this is an animation, so it's frames uv coords are stored in an array
var bubblePopUVCoords = new Array(8);
bubblePopUVCoords[7] = new Rectangle(1.5, 294.5, 122.2, 121.9);
bubblePopUVCoords[6] = new Rectangle(137.1, 306.0, 110.9, 110.4);
bubblePopUVCoords[5] = new Rectangle(375.3, 225, 99.65, 98.9);
bubblePopUVCoords[4] = new Rectangle(275.2, 225, 88.35, 87.5);
bubblePopUVCoords[3] = new Rectangle(187.2, 225, 77.05, 76.05);
bubblePopUVCoords[2] = new Rectangle(114.1, 225, 65.75, 64.6);
bubblePopUVCoords[1] = new Rectangle(55.05, 225, 54.5, 53.1);
bubblePopUVCoords[0] = new Rectangle(1.6, 225, 43.2, 41.7);


// just so don't have to recalculate these over and over...
var popFrameOffsets = new Array(8);
popFrameOffsets[7] = new Point(bubblePopUVCoords[7].width / 2, bubblePopUVCoords[7].height / 2);
popFrameOffsets[6] = new Point(bubblePopUVCoords[6].width / 2, bubblePopUVCoords[6].height / 2);
popFrameOffsets[5] = new Point(bubblePopUVCoords[5].width / 2, bubblePopUVCoords[5].height / 2);
popFrameOffsets[4] = new Point(bubblePopUVCoords[4].width / 2, bubblePopUVCoords[4].height / 2);
popFrameOffsets[3] = new Point(bubblePopUVCoords[3].width / 2, bubblePopUVCoords[3].height / 2);
popFrameOffsets[2] = new Point(bubblePopUVCoords[2].width / 2, bubblePopUVCoords[2].height / 2);
popFrameOffsets[1] = new Point(bubblePopUVCoords[1].width / 2, bubblePopUVCoords[1].height / 2);
popFrameOffsets[0] = new Point(bubblePopUVCoords[0].width / 2, bubblePopUVCoords[0].height / 2);



// UI positions
var hpPosition = new Point(730, 575);
var scorePosition = new Point(40, 575);
var soundIconPosition = new Point(775, 575);

// this is for the position offset for a bubble bullet's combo display
var comboLabelOffset = new Point(numberUVCoords.width * BUBBLE_COMBO_LABEL_SIZE / 2, numberUVCoords.height * BUBBLE_COMBO_LABEL_SIZE / 4);

// these are for the game over screen's score display
var playerScorePosition = new Point(190, 464);
var playerKillCountPosition = new Point(415, 464);
var playerComboPosition = new Point(640, 464);

function drawSoundIcon(inputGraphics, state){
	
	var useCoords;
	if(state == true){
		useCoords = soundOnIcon;
	} else {
		useCoords = soundOffIcon;
	}
	
	inputGraphics.drawImage(bubbleGraphics, useCoords.x, useCoords.y, 
							useCoords.width, useCoords.height,
							soundIconPosition.x - (useCoords.width / 2), soundIconPosition.y - (useCoords.height / 2), 
							useCoords.width, useCoords.height);
}


function drawBackground(inputGraphics){
	inputGraphics.drawImage(imageLoader.getImage(BACKGROUND_IMAGE), 0, 0);	
}

function drawStartScreen(inputGraphics){
	inputGraphics.drawImage(imageLoader.getImage(PRE_GAME_IMAGE), 0, 0);
}

function drawGameOver(inputGraphics){
	inputGraphics.drawImage(imageLoader.getImage(GAME_OVER_IMAGE), 0, 0);
}


function drawPlayer(inputGraphics, inputPlayerSprite){
	var position = inputPlayerSprite.getPos();
	
	// draw a littke character inside the player bubble
	inputGraphics.drawImage(bubbleGraphics, guyUVCoords.x, guyUVCoords.y, 
						guyUVCoords.width, guyUVCoords.height,
						position.x - (guyUVCoords.width / 2 * PLAYER_ICON_SIZE), 
						position.y - (guyUVCoords.height / 2 * PLAYER_ICON_SIZE), 
						guyUVCoords.width * PLAYER_ICON_SIZE, guyUVCoords.height * PLAYER_ICON_SIZE);
	
	// "enemyGraphicOffset" is used as the enemy and player bubbles are the same size
	inputGraphics.drawImage(bubbleGraphics, blueBubbleUVCoords.x, blueBubbleUVCoords.y, 
							blueBubbleUVCoords.width, blueBubbleUVCoords.height,
							position.x - enemyGraphicOffset, position.y - enemyGraphicOffset, 
							PLAYER_SIZE, PLAYER_SIZE);
}


function drawEnemySprites(inputGraphics, inputSprites){
	
	var position;
	var useCoords;
	
	for(var sprite in inputSprites){
		
		position = inputSprites[sprite].getPos();
		
		// draw a littke character inside the enemy bubble
		// same size as player icon
		inputGraphics.drawImage(bubbleGraphics, dinoUVCoords.x, dinoUVCoords.y, 
						dinoUVCoords.width, dinoUVCoords.height,
						position.x - (dinoUVCoords.width / 2 * PLAYER_ICON_SIZE), 
						position.y - (dinoUVCoords.height / 2 * PLAYER_ICON_SIZE), 
						dinoUVCoords.width * PLAYER_ICON_SIZE, dinoUVCoords.height * PLAYER_ICON_SIZE);
		
		// based on the enemy bubble's hp, choose the UV coords for a coloured bubble
		switch(inputSprites[sprite].hp){
			case 1:
				useCoords = redBubbleUVCoords;
				break;
			case 2:
				useCoords = orangeBubbleUVCoords;
				break;
			case 3:
				useCoords = yellowBubbleUVCoords;
				break;
			case 4:
				useCoords = greenBubbleUVCoords;
				break;
			default:
				console.log("drawEnemySprites() - Unknown colour to use to display enemy bubble. Hp: "+ inputSprites.hp);
				useCoords = redBubbleUVCoords;
				break;
		}
		
		inputGraphics.drawImage(bubbleGraphics, useCoords.x, useCoords.y, 
								useCoords.width, useCoords.height,
								position.x - enemyGraphicOffset, position.y - enemyGraphicOffset, 
								ENEMY_SIZE, ENEMY_SIZE);
	}
}


function drawNumberText(inputGraphics, inputValue, position, size, centered){
	var valueString = inputValue.toString();
	
	var currentX = position.x;
	var currentY = position.y;
	
	// if the text is to be centered, move the starting position by half the string length * char size
	if(centered == true){
		currentX -= (valueString.length / 2) * (numberUVCoords.width * size);
	}
	
	
	// this is the uv coord for the number 0 graphic
	// for each number use this rectangle and offset by the width * number
	var uvOffset = 0;
	
	var glyphWidthOffset = numberUVCoords.width / 2 * size;
	var glyphHeightOffset = numberUVCoords.height / 2 * size;
	
	
	// for each character calculate the UV coord offset needed, then draw it
	for(var counter = 0; counter < valueString.length; counter++){
		// parse the char back in to a number as it is used to calculate the offset
		uvOffset = parseInt(valueString.charAt(counter), 10) * numberUVCoords.width;
		
		inputGraphics.drawImage(bubbleGraphics, numberUVCoords.x + uvOffset, numberUVCoords.y, 
								numberUVCoords.width, numberUVCoords.height,
								currentX - glyphWidthOffset, currentY - glyphHeightOffset, 
								numberUVCoords.width * size, numberUVCoords.height * size);
		
		currentX += (numberUVCoords.width * size);
	}
}


// player hp is drawn as a set of smaller bubbles below the player
function drawPlayerHP(inputGraphics, inputHp){
	
	var currentX = hpPosition.x;
	var currentY = hpPosition.y;
	var spacing = ENEMY_SIZE * 0.5;
	var offset = (ENEMY_SIZE * 0.25) / 2;
	
	// draw 1 less then the hp, since visually they look like they represent 'lives' more then hp
	for(var counter = 0; counter < (inputHp - 1); counter++){
		
		inputGraphics.drawImage(bubbleGraphics, blueBubbleUVCoords.x, blueBubbleUVCoords.y, 
								blueBubbleUVCoords.width, blueBubbleUVCoords.height,
								currentX - offset, currentY - offset,
								ENEMY_SIZE * 0.25, ENEMY_SIZE * 0.25);
		
		currentX -= spacing;
	}
}

function drawBullets(inputGraphics, inputSprites){
	
	var position;
	
	for(var sprite in inputSprites){
		
		position = inputSprites[sprite].getPos();
		
		if(inputSprites[sprite].owner == ENEMY_ID){
			
			// could also draw the different coloured enemy's bubble bullets in different colours as well
			// but it may seem a bit confusing as they all have the same effect, so keep them all red
			inputGraphics.drawImage(bubbleGraphics, redBubbleUVCoords.x, redBubbleUVCoords.y, 
									redBubbleUVCoords.width, redBubbleUVCoords.height,
									position.x - bulletGraphicOffset, position.y - bulletGraphicOffset, 
									BULLET_SIZE, BULLET_SIZE);
			
		} else if(inputSprites[sprite].owner == PLAYER_ID){
			inputGraphics.drawImage(bubbleGraphics, blueBubbleUVCoords.x, blueBubbleUVCoords.y, 
									blueBubbleUVCoords.width, blueBubbleUVCoords.height,
									position.x - bulletGraphicOffset, position.y - bulletGraphicOffset,
									BULLET_SIZE, BULLET_SIZE);
		} else {
			console.log("Unknown bubble bullet type to draw");
		}
		
		// if the bubble's local combo/kill count is higher enough, draw a little number to keep track of it for the player
		if(inputSprites[sprite].comboCounter >= COMBO_DISPLAY_THRESHOLD){
			
			drawNumberText(inputGraphics, inputSprites[sprite].comboCounter, 
							new Point(position.x + comboLabelOffset.x, position.y + comboLabelOffset.y), 
							BUBBLE_COMBO_LABEL_SIZE, true);
		}
	} // end of for loop
}



// reference: http://creativejs.com/2012/01/day-10-drawing-rotated-images-into-canvas/
function drawWalls(inputGraphics, inputSprites){ 
	for(var wall in inputSprites){
		drawWall(inputGraphics, inputSprites[wall]);
	}
}

// separated out a bit as it seemed more complex to draw a single rotated graphic
// also a bit easier to test with a single call
function drawWall(inputGraphics, inputWallSprite){
	
	var position = inputWallSprite.getPos();
	
	var width = inputWallSprite.width;
	
	// save the current canvas coordinate system before we mess with it
	inputGraphics.save(); 
	
	// move to the middle of where we want to draw our image
	inputGraphics.translate(position.x, position.y);
	
	// rotate around that point
	inputGraphics.rotate(inputWallSprite.rotation);
	
	// then draw it
	inputGraphics.drawImage(bubbleGraphics, wallUVCoords.x, wallUVCoords.y, 
									wallUVCoords.width, wallUVCoords.height,
									0 - (width / 2), 0 - (WALL_THICKNESS / 2),
									width, WALL_THICKNESS);
	
	// and restore the canvas coordinate system to how they were
	inputGraphics.restore(); 
}


function drawBubblePopSprites(inputGraphics, popSprites){
	var frame;
	var size;
	
	var position;
	
	//for(var counter = 0; counter < popSprites.length; counter++){
	for(var sprite in popSprites){
		frame = popSprites[sprite].currentFrame;
		position = popSprites[sprite].getPos();
		
		if(popSprites[sprite].isBullet == true){
			size = BULLET_EXPLODE_SIZE;
		} else {
			size = ENEMY_EXPLODE_SIZE;
		}
		
		inputGraphics.drawImage(bubbleGraphics, bubblePopUVCoords[frame].x, bubblePopUVCoords[frame].y, 
							bubblePopUVCoords[frame].width, bubblePopUVCoords[frame].height,
							position.x - (popFrameOffsets[frame].x * size), position.y - (popFrameOffsets[frame].y * size),
							bubblePopUVCoords[frame].width * size, bubblePopUVCoords[frame].height * size);
	}
}
