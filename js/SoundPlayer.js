// this is a basic SoundPlayer{} class
// It loads, stores, and plays sounds.

function SoundPlayer(){
	var self = {};
	
	try {
		self.context = new webkitAudioContext();
		
	} catch (exception) {
		alert("Web Audio API is not supported in this browser");
	}
	
	self.mainVolumeNode = self.context.createGainNode(0);
	self.mainVolumeNode.connect(self.context.destination);
	
	// volume starts at 1, keep track of this value to reuse when muting and un-muting
	// as if it was just relying on the mainVolumeNode value, it would only be 1 or 0
	self.volume = 1;
	
	self.sounds = {};
	
	self.loadSound = function(inputURL, callback){
		
		// check if a sound object exists here... possible it can be already loaded or still loading
		// in both cases ignore it and return
		if(self.sounds[inputURL] != null){
			return;
		}
		
		self.sounds[inputURL] = {};
		
		var request = new XMLHttpRequest();
		request.open("GET", inputURL, true);
		request.responseType = "arraybuffer";
		request.onload = function(inputEvent) {
			
			// decode the audio data and stick it in to the sound buffer, also mark it as loaded
			self.context.decodeAudioData(request.response,
			function (buffer) {
				self.sounds[inputURL].buffer = buffer;
				self.sounds[inputURL].loaded = true;
				if(callback != null){
					callback(inputURL);
				}
			}, function(data){});
			
		};
		request.send();
	}
	
	self.play = function(inputURL){
		var selectedSound = self.sounds[inputURL];
		
		// if the sound is not loaded, load it and pass in the play() method as a call back so it'll play right away when done loading
		if((selectedSound == null) || (selectedSound.loaded == false)){
			self.loadSound(inputURL, self.play);
			return;
		}
		
		var currentClip = self.context.createBufferSource();

		// Set the properties of currentClip appropriately in order to play the sound
		currentClip.buffer = selectedSound.buffer;
		currentClip.gain.value = 1; //self.volume;
		currentClip.loop = false;

		// Connect currentClip to the main node, then play it. We can do
		// this using the 'connect' and 'noteOn' methods of currentClip.
		currentClip.connect(self.mainVolumeNode);
		currentClip.noteOn(0);
	}
	
	self.toggleMute = function(){
		if(self.mainVolumeNode.gain.value > 0) {
			self.mainVolumeNode.gain.value = 0;
		} else {
			self.mainVolumeNode.gain.value = self.volume;
		}
	}
	
	self.setVolume = function(input){
		// remember this volume level in case toggle mute is used, then it can be used to restore thier desired volume, rather then to max again
		self.volume = input;
		self.mainVolumeNode.gain.value = input;
	}
	
	self.getVolume = function(){
		return self.mainVolumeNode.gain.value;
	}
	
	return self;
}