// Asset constants - all asset constants are URLs to that asset as well ------------
// =================================================================================

var BACKGROUND_IMAGE = "graphics/greyWashBackground.jpg";
var SPRITE_GRAPHICS_IMAGE = "graphics/bubbleGraphics.png";
var PRE_GAME_IMAGE = "graphics/startScreen.png";
var GAME_OVER_IMAGE = "graphics/gameOver.png";

var POP_SOUND = "sounds/pop.mp3";
var BUBBLE_SHOOTING = "sounds/shooting02.mp3";
var PLAYER_HIT = "sounds/playerHit.mp3";
var GAME_OVER_SOUND = "sounds/gameOver.mp3";



// Game Rendering related constants ------------------------------------------------
// =================================================================================

var BULLET_SIZE = 50; // 50 pixels
var ENEMY_SIZE = 75; // 75 pixels
var PLAYER_SIZE = 75; // 75 pixels

// determines how many bubbles a bubble bullet needs to pop before it displays its combo value
var COMBO_DISPLAY_THRESHOLD = 3;

var PLAYER_ICON_SIZE = 0.75; // these are percentages of the original sprite size

var BULLET_EXPLODE_SIZE = 0.50; // these are percentages of the original sprite size
var ENEMY_EXPLODE_SIZE = 0.75; // these are percentages of the original sprite size

var GAME_OVER_SCORE_FONT_SIZE = 0.8; // these are percentages of the original bitmap font's size
var SCORE_FONT_SIZE = 1; // these are percentages of the original bitmap font's size
var BUBBLE_COMBO_LABEL_SIZE = 0.4;

var WALL_GUIDE_COLOUR = "#99FFFF"; // a nice cyan
var WALL_GUIDE_THICKNESS = 1;



// Game engine constants -----------------------------------------------------------
// =================================================================================

var UPDATE_RATE = 60; // 60 fps

var PHYSICS_UPDATE_RATE = 1 / UPDATE_RATE;

var PHYSICS_SCALE = 100; // 100 pixels = 1 meter

// owner group Ids, used to determine collision groups along side sprite Ids as sometimes the game cares
// about particular sprite types, and other times it only cares about the sprite's owner
var ENEMY_ID = 1;
var PLAYER_ID = 2;
// no side owns walls, this also enables the player's own bullets to hit the wall again
// to disable the player's own bubble bullets from rebounding on a wall (so bubbles 
// only bounce once on a player's wall), set this to the same value as PLAYER_ID
var WALL_ID = 0;


// id's that are used to determine what sprite is what
var ENEMY_SPRITE_ID = 1;
var ENEMY_BULLET_ID = 2;
var PLAYER_WALL_SPRITE_ID = 3;
var PLAYER_BULLET_ID = 4;
var PLAYER_SPRITE_ID = 5;
var POP_SPRITE_ID = 6;



// Gameplay constants --------------------------------------------------------------
// =================================================================================

// the limit on how far sprites can move out of bounds
var BOUNDS = 100;

var BULLET_SPEED = 4; // speed per tick

// how thick a wall is and how long can the player make a wall
var WALL_THICKNESS = 10; // in pixels
var WALL_LENGTH_LIMIT = 75; // in pixels

// measured in ticks, lasts for 60 ticks which is 1 second
var WALL_AGE = 60;

var STARTING_ENEMY_LIMIT = 5;
var STARTING_ENEMY_HP = 1;

var PLAYER_STARTING_HP = 6; // 6 for actual, 50 for testing

var STARTING_ENEMY_COUNT = 5; // starting amount of enemies that can be on screen at any time
var ENEMY_COUNT_INCREASE_RATE = 5; // every 5 kills, increase the amount of enemies allowed by 1

var ENEMY_HP_INCREASE_RATE = 10; // every 5 kills, increase the hp of enemies allowed by 1
var ABSOLUTE_MAX_ENEMY_HP = 4; // max enemy hp is 4...this is due to the fact that there are only 4 colour bubble graphics to represent it

var ENEMY_SHOOT_INCREASE_RATE = 5; // increase enemy shooting rate by 5 ticks

// every 30 ticks an enemy will shoot when game is maxed, keep in mind that the tick rate is 60
// a value of 30 would mean every half second a bullet bubble can be shot
var MAX_ENEMY_SHOOTING_SPEED = 30;