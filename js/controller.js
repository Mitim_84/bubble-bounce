


// draws a rectangle at any rotation...probably a bit complex for just one function, but following the KIS rule for now
// points are transformed using a rotation matrix
function drawRectangle(inputGraphics, inputLength, inputPosition, inputRotation){
	
	var sin = Math.sin(inputRotation);
	var cos = Math.cos(inputRotation);
	
	var currentPoint = new Point(0, 0);
	var rotatedPoint = new Point(0, 0);
	
	// moving to top left point
	currentPoint.x = -inputLength - (WALL_THICKNESS / 2);
	currentPoint.y = -(WALL_THICKNESS / 2);
	rotatedPoint.x = currentPoint.x * cos - currentPoint.y * sin;
	rotatedPoint.y = currentPoint.x * sin + currentPoint.y * cos;
	inputGraphics.moveTo(rotatedPoint.x + inputPosition.x, rotatedPoint.y + inputPosition.y);
	
	// drawing to top right point
	currentPoint.x = inputLength - (WALL_THICKNESS / 2);
	currentPoint.y = -(WALL_THICKNESS / 2);
	rotatedPoint.x = currentPoint.x * cos - currentPoint.y * sin;
	rotatedPoint.y = currentPoint.x * sin + currentPoint.y * cos;
	inputGraphics.lineTo(rotatedPoint.x + inputPosition.x, rotatedPoint.y + inputPosition.y);
	
	// drawing to bottom right point
	currentPoint.x = inputLength - (WALL_THICKNESS / 2);
	currentPoint.y = (WALL_THICKNESS / 2);
	rotatedPoint.x = currentPoint.x * cos - currentPoint.y * sin;
	rotatedPoint.y = currentPoint.x * sin + currentPoint.y * cos;
	inputGraphics.lineTo(rotatedPoint.x + inputPosition.x, rotatedPoint.y + inputPosition.y);
	
	// drawing to bottom left point
	currentPoint.x = -inputLength - (WALL_THICKNESS / 2);
	currentPoint.y = (WALL_THICKNESS / 2);
	rotatedPoint.x = currentPoint.x * cos - currentPoint.y * sin;
	rotatedPoint.y = currentPoint.x * sin + currentPoint.y * cos;
	inputGraphics.lineTo(rotatedPoint.x + inputPosition.x, rotatedPoint.y + inputPosition.y);
	
	// drawing to top left point
	currentPoint.x = -inputLength - (WALL_THICKNESS / 2);
	currentPoint.y = -(WALL_THICKNESS / 2);
	rotatedPoint.x = currentPoint.x * cos - currentPoint.y * sin;
	rotatedPoint.y = currentPoint.x * sin + currentPoint.y * cos;
	inputGraphics.lineTo(rotatedPoint.x + inputPosition.x, rotatedPoint.y + inputPosition.y);
} // end of drawRectangle() method


// booleans used to keep track of the mouse input state and when a wall is being drawn and when it is done drawing
var drawPendingWall = false;
var wallCreation = false;

// keeps track of the mouse down and up points for wall drawing
var wallStartPoint = new Point(0, 0);
var wallDragPoint = new Point(0, 0);

// used to store any pending walls to be made (the wall 'wireframe/guide' drawing)
var pendingWall = new Rectangle();
pendingWall.height = WALL_THICKNESS;

// while the mouse is down and in a state of wall creation, this method calculates the wall position, angle, and length
// then calls a method to draw the wall guide
function drawBuildWallGuide(inputGraphics){
	if(wallCreation == true){
		var angle = Math.atan2(wallStartPoint.y - wallDragPoint.y, wallStartPoint.x - wallDragPoint.x);
		
		var position = new Point(wallStartPoint.x, wallStartPoint.y);
		
		var length = (wallStartPoint.x - wallDragPoint.x) * (wallStartPoint.x - wallDragPoint.x);
		length += (wallStartPoint.y - wallDragPoint.y) * (wallStartPoint.y - wallDragPoint.y);
		length = Math.sqrt(length);
		
		// cap the length to a limit
		length = ((length < WALL_LENGTH_LIMIT) ? (length) : (WALL_LENGTH_LIMIT));
		
		drawRectangle(inputGraphics, length, wallStartPoint, angle);
		inputGraphics.stroke();
		
		// while the drawing is going on, also update the pendingWall rectangle so 
		// it can be passed to the game to create an actual wall when the user is done drawing
		pendingWall.width = length * 2;
		pendingWall.rotation = angle;
		pendingWall.x = position.x;
		pendingWall.y = position.y;
	}
}


function onMouseDrag(inputEvent){
	wallDragPoint.x = inputEvent.clientX;
	wallDragPoint.y = inputEvent.clientY;
}

function onMouseDown(inputEvent){
	
	drawPendingWall = false;
	wallCreation = true;
	
	wallStartPoint.x = inputEvent.clientX;
	wallStartPoint.y = inputEvent.clientY;
	
	wallDragPoint.x = inputEvent.clientX;
	wallDragPoint.y = inputEvent.clientY;
	
	canvasTag.addEventListener("mousemove", onMouseDrag, false);
}
canvasTag.addEventListener("mousedown", onMouseDown, false);


// when mouse up the wall is done creating
function onMouseUp(inputEvent){
	wallCreation = false;
	canvasTag.removeEventListener("mousemove", onMouseDrag, false);
	
	// flag this to create a wall in the game's main update loop
	// do Not create it here directly as it may run into some race condition (physics engine may be in mid-step)
	drawPendingWall = true;
}
canvasTag.addEventListener("mouseup", onMouseUp, false);


// this handles the clicking of the mouse button on the bottom right corner where the mute button is
function onMouseClick(inputEvent){
	// can cheat a bit here, since the game only has 1 button (which is the mute button) in the bottom right corner
	// the hit area is 40x40 big so can just manually check for that
	if(inputEvent.clientX > (canvasTag.width - 40)){
		if(inputEvent.clientY > (canvasTag.height - 40)){
			soundPlayer.toggleMute();
		}
	}
}
canvasTag.addEventListener("click", onMouseClick, false);