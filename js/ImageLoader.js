// only loads images...sounds will be loaded async and played when they are available
function ImageLoader(){
	var self = {};
	
	self.assets = {};
	
	self.loadingQueue = new Array();
	
	self.queueAsset = function(inputURL, callback){
		self.loadingQueue.push(inputURL);
		self.processQueue(callback);
	}
	
	self.processQueue = function(callback){
		
		if(self.loadingQueue.length == 0){
			if(callback != null){
				callback();
			}
			
		} else {
			var url = self.loadingQueue.pop();
			var image = new Image();
			image.onload = function(){
				self.processQueue(callback);
			}
			self.assets[url] = image;
			image.src = url;
		}
	}
	
	self.getImage = function(url){
		return self.assets[url];
	}
	
	return self;
}