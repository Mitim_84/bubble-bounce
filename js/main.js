/*

This is the main game code.

It contains code to create sprites, solve collsions when they arise, and the main game loop
- create enemies
- create blocks
- create physics
- update loop
- handle collisions
- handle rendering calls

It also contains a few commented out sounds left in on purpose.
*/

var gameIntervalId;

var canvasTag = document.getElementById("canvasTag");
var graphics = canvasTag.getContext("2d");

// for convenience
graphics.clear = function(){
	this.clearRect(0, 0, canvasTag.width, canvasTag.height);
	this.beginPath();
}


var soundPlayer = new SoundPlayer();
soundPlayer.loadSound(POP_SOUND);
//soundPlayer.loadSound(BUBBLE_SHOOTING);
soundPlayer.loadSound(PLAYER_HIT);
soundPlayer.loadSound(GAME_OVER_SOUND);

var gameState = new GameState();

var physicsEngine = new PhysicsEngine();
physicsEngine.setDebugDraw(graphics);

// create a triangle that covers the stage (leaving the bottom right corner free) for a random enemy spawn area
var enemySpwanArea = new Triangle(new Point(0, 0), new Point(canvasTag.width, 0), new Point(0, canvasTag.height));

var enemySprites = new Array();

// this is hardcoded as the first enemy always at this point
var firstEnemy = new EnemySprite(1);
firstEnemy.physBody = physicsEngine.addEnemy(firstEnemy, new Point(100, 100));
enemySprites.push(firstEnemy);


var playerSprite = new Sprite(PLAYER_SPRITE_ID, PLAYER_ID, 0);
playerSprite.physBody = physicsEngine.addPlayer(playerSprite, new Point(canvasTag.width - 100, canvasTag.height - 100));


// these are the animated sprites for when bubbles pop
var popSprites = new Array();
var wallSprites = new Array();
var bulletSprites = new Array();

function createBubblePopSprite(inputPos, isBullet){
	popSprites.push(new PoPSprite(inputPos.x, inputPos.y, isBullet));
}

// this goes and updates each bubble pop sprite and removes any that have expired / are done playing
function updateBubblePopSprites(){
	for(var counter = 0; counter < popSprites.length; counter++){
		if(popSprites[counter].donePlaying == true){
			popSprites.splice(counter, 1);
			counter--;
		} else {
			popSprites[counter].update();
		}
	}
}

function createEnemy(){
	var point = enemySpwanArea.getRandomPoint();
	var newEnemy = new EnemySprite(gameState.getRandomEnemyHp());
	newEnemy.physBody = physicsEngine.addEnemy(newEnemy, point);
	enemySprites.push(newEnemy);
}

function createBullet(inputPosition, inputTargetPoint){
	
	var xDist = inputTargetPoint.x - inputPosition.x;
	var yDist = inputTargetPoint.y - inputPosition.y;
	
	var velocity = new B2Vec2(xDist, yDist);
	velocity.Normalize();
	velocity.Multiply(BULLET_SPEED);
	
	var result = new BulletSprite(ENEMY_BULLET_ID, ENEMY_ID, bulletSprites.length);
	result.physBody = physicsEngine.addBullet(result, inputPosition, velocity);
	bulletSprites.push(result);
	
	// this is actually a bit annoying when there are many enemies shooting
	//soundPlayer.play(BUBBLE_SHOOTING);
	
	return result;
}






// used to remove a generic sprite
function removeSprite(inputSprite){
	
	if(inputSprite.hitObject != null){
		inputSprite.hitObject = null;
	}
	
	var temp = inputSprite.physBody;
	inputSprite.physBody = null;
	physicsEngine.destroyBody(temp);
}

// checks and removes any walls that have expired
function checkAndRemoveOldWalls(){
	for(var counter = 0; counter < wallSprites.length; counter++){
		if(wallSprites[counter].updateAge() == false){
			removeSprite(wallSprites[counter]);
			wallSprites.splice(counter, 1);
			counter--;
		}
	}
}

// scan through and look for bullets that hit something, then handle the different cases
// at the same time, fix any 'slow' bullets due to a glitch (see BulletSprite class)
function resolveHitBullets(){
	for(var counter = 0; counter < bulletSprites.length; counter++){
		
		bulletSprites[counter].fixSpeed();
		
		// if a bubble bubble's hitObject is not null, it means it has hit something, handle it now
		if(bulletSprites[counter].hitObject != null){
			
			// if hit a wall, don't do anything, just clear it's hitObject property
			if(bulletSprites[counter].hitObject.type == PLAYER_WALL_SPRITE_ID){
				bulletSprites[counter].hitObject = null;
				continue;
			}
			
			// else it must have hit something interesting: either an enemy or a player
			var hitSprite = bulletSprites[counter].hitObject;
			// need to clear this right away to avoid processing a double hit
			bulletSprites[counter].hitObject = null;
			
			if(hitSprite.type == PLAYER_SPRITE_ID){
				
				createBubblePopSprite(bulletSprites[counter].getPos(), true);
				
				removeSprite(bulletSprites[counter]);
				bulletSprites.splice(counter, 1);
				counter--;
				
				gameState.addEnemyHit();
				
				// the actual logic for handling game over is done in the main game update loop
				// as it can be happening during an update step, which is unpredictable
				// sound however can be handled in here as it is async and doesn't depend on anything
				// (while the other game over logic depends on the screen drawing)
				if(gameState.getPlayerHp() < 1){
					soundPlayer.play(GAME_OVER_SOUND);
					
				} else {
					soundPlayer.play(PLAYER_HIT);
				}
				continue;
			}
			
			// if it hits an enemy, call it's damage() method
			// if it returns false, then it means the enemy has 0 hp, else it just got damaged but still exists
			if((hitSprite.type == ENEMY_SPRITE_ID) && (hitSprite.damage() == false)){
				
				// find the matching enemy sprite and remove it from the list
				var removalIndex = enemySprites.indexOf(hitSprite);
				enemySprites.splice(removalIndex, 1);
				
				createBubblePopSprite(hitSprite.getPos(), false);
				
				removeSprite(hitSprite);
				
				// ~after it hits an enemy, it is an enemy bullet now...hm.. not too sure about this as
				// it can be fun to keep it a player bullet; can gain multiple kills with one bubble this way
				//bulletSprites[counter].owner = ENEMY_ID;
				
				gameState.addPlayerKill();
				soundPlayer.play(POP_SOUND);
				
				gameState.addScore(hitSprite.pointValue);
				
				// since the bullet hit something, increment it to keep track of how many time's it has hit something
				bulletSprites[counter].comboCounter++;
				// also update the game state so it can be displayed at the end
				gameState.updateCombo(bulletSprites[counter].comboCounter);
				
				// this is currently not used, but left in as it may be implemented later
				// ~can choose to either remove or not remove bullets that killed enemies based on enemy
				/*if(hitSprite.reflectBullet == false){
					removeSprite(bulletSprites[counter]);
					bulletSprites.splice(counter, 1);
					counter--;
				}*/
				continue;
			}
		}
	}
} // end of resolveHitBullets() method

// this checks if bullets have gone out of bounds, then removes them if they have
function checkOutOfBoundBullets(){
	var temp;
	for(var counter = 0; counter < bulletSprites.length; counter++){
		temp = bulletSprites[counter].getPos();
		if(((temp.x < -BOUNDS) || (temp.x > canvasTag.width + BOUNDS)) || ((temp.y < -BOUNDS) || (temp.y > canvasTag.height + BOUNDS))){
			removeSprite(bulletSprites[counter]);
			bulletSprites.splice(counter, 1);
			counter--;
		}
	}
}



// this is the game's main update loop
function update(){
	
	graphics.clear();
	
	// update the game state's counters
	gameState.update();
	
	// create a random enemy if the gameState object decides it's time
	if(gameState.timeToSpawnEnemy() == true){
		if(enemySprites.length < gameState.maxEnemies){
			createEnemy();
		}
	}
	
	// create a random bullet from a random enemy if the gameState object decides it's time
	if(gameState.timeToSpawnBullet() == true){
		// only if there are enemies, if not then do nothing, just reset the counter for this part
		if(enemySprites.length > 0){
			var randomIndex = Math.floor(Math.random() * enemySprites.length);
			var position = enemySprites[randomIndex].getPos();
			if((position.x == 0) && (position.y == 0)){
				console.log(randomIndex, position, enemySprites);
			}
			
			createBullet(position, playerSprite.getPos());
		}
	}
	
	physicsEngine.step();
	
	// always draw the background first
	drawBackground(graphics);
	
	
	// left this in for debugging
	//physicsEngine.drawDebug();
	
	// set the wall guide drawing colour and thickness
	graphics.strokeStyle = WALL_GUIDE_COLOUR;
	graphics.lineWidth = WALL_GUIDE_THICKNESS;
	
	drawBuildWallGuide(graphics);
	
	// if this boolean is true (set from the mouse controller code), then
	// it means the player has just released their mouse while creating a wall
	// thus create the actual wall physically now
	if(drawPendingWall == true){
		
		var newWall = new WallSprite(pendingWall, wallSprites.length);
		newWall.physBody = physicsEngine.addWall(newWall, pendingWall);
		wallSprites.push(newWall);
		
		drawPendingWall = false;
	}
	
	checkAndRemoveOldWalls();
	checkOutOfBoundBullets();
	// should remove the out-of-bound bullets first, so can skip processing on them
	resolveHitBullets();
	
	updateBubblePopSprites();
	
	// draw the rest of the graphics
	drawBullets(graphics, bulletSprites);
	drawEnemySprites(graphics, enemySprites);
	
	drawNumberText(graphics, gameState.getPlayerScore(), scorePosition, SCORE_FONT_SIZE, false);
	drawWalls(graphics, wallSprites);
	
	drawBubblePopSprites(graphics, popSprites);
	
	drawSoundIcon(graphics, soundPlayer.getVolume() > 0);
	
	if(gameState.getPlayerHp() < 1){
		endGame();
	} else {
		drawPlayer(graphics, playerSprite);
		drawPlayerHP(graphics, gameState.getPlayerHp());
	}
} // end of update() method




function startGame(inputEvent){
	canvasTag.removeEventListener("click", startGame, false);
	gameIntervalId = setInterval(update, 1000 / UPDATE_RATE);
}

function endGame(){
	clearInterval(gameIntervalId);
	
	// do not clear the graphics, just draw the game over screen on top
	
	// manually create this graphic for the player bubble popping as need to set the initial frame
	var playerPos = playerSprite.getPos();
	var playerPopSprite = new PoPSprite(playerPos.x, playerPos.y, false);
	playerPopSprite.currentFrame = 6; // set this to the second last frame
	popSprites.push(playerPopSprite);
	drawBubblePopSprites(graphics, popSprites);
	
	drawGameOver(graphics);
	
	// draw the player's score, kill count, and combo count
	drawNumberText(graphics, gameState.getPlayerScore(), playerScorePosition, GAME_OVER_SCORE_FONT_SIZE, true);
	drawNumberText(graphics, gameState.getPlayerKillCount(), playerKillCountPosition, GAME_OVER_SCORE_FONT_SIZE, true);
	drawNumberText(graphics, gameState.getPlayerCombo(), playerComboPosition, GAME_OVER_SCORE_FONT_SIZE, true);
}

// pregame stuff / the start screen
function startPreGame(){

	canvasTag.addEventListener("click", startGame, false);
	drawBackground(graphics);
	
	drawPlayer(graphics, playerSprite);
	drawPlayerHP(graphics, gameState.getPlayerHp());
	
	drawStartScreen(graphics);
}



// this determines what objects are "allowed" to collide and what collisions are ignored
// - disable collisions between enemy vs enemy objects 
// - disable collisions player vs player objects
// - enable them between player vs enemy objects
var contactListener = new B2ContactListener();

// this is called just as bodies are beginning to collide so they can be set to ignore each other and continue on or not
contactListener.PreSolve = function(contact, oldManifold){
	var bodyASprite = contact.GetFixtureA().GetBody().GetUserData();
	var bodyBSprite = contact.GetFixtureB().GetBody().GetUserData();
	
	// bullets should never collide
	if(((bodyASprite.type == ENEMY_BULLET_ID) || (bodyASprite.type == PLAYER_BULLET_ID)) && 
		((bodyBSprite.type == ENEMY_BULLET_ID) || (bodyBSprite.type == PLAYER_BULLET_ID))){

		contact.SetEnabled(false);
	
	// else all things collide if they belong to differing player vs enemy
	} else {
		contact.SetEnabled(bodyASprite.owner != bodyBSprite.owner);
	}
}

// this is called just after bodies have finished colliding
contactListener.EndContact = function(contact){
	
	var bodyASprite = contact.GetFixtureA().GetBody().GetUserData();
	var bodyBSprite = contact.GetFixtureB().GetBody().GetUserData();
	
	// if an enemy bullet was hit/reflected by the player's wall, change them to belong to the player
	if((bodyASprite.type == PLAYER_WALL_SPRITE_ID) && (bodyBSprite.type == ENEMY_BULLET_ID)){
		bodyBSprite.owner = PLAYER_ID;
		bodyBSprite.type = PLAYER_BULLET_ID;
	
	// same as before but with A and B swapped
	} else if((bodyBSprite.type == PLAYER_WALL_SPRITE_ID) && (bodyASprite.type == ENEMY_BULLET_ID)){
		bodyASprite.owner = PLAYER_ID;
		bodyASprite.type = PLAYER_BULLET_ID;
	}
}

// after all collisions are done, set their 'hitSprite' properties so they can
// be handled later by the game code in sequence in the game loop...usually this only applies to bubble bullets
contactListener.PostSolve = function(contact, impulse){
	var bodyASprite = contact.GetFixtureA().GetBody().GetUserData();
	var bodyBSprite = contact.GetFixtureB().GetBody().GetUserData();
	
	// if a sprite is a bullet, then set it's hitObject property to indicate it hit something
	if((bodyASprite.type == PLAYER_BULLET_ID) || (bodyASprite.type == ENEMY_BULLET_ID)){
		bodyASprite.hitObject = bodyBSprite;
	}
	
	// same as before but with A and B swapped
	if((bodyBSprite.type == PLAYER_BULLET_ID) || (bodyBSprite.type == ENEMY_BULLET_ID)){
		bodyBSprite.hitObject = bodyASprite;
	}	
}

physicsEngine.setContactListener(contactListener);
