// this contains various game related classes

// this is used as a base Sprite class
function Sprite(inputType, inputOwner){
	var self = {};
	self.physBody = null;
	
	self.owner = inputOwner || -2; // any negatives are error values
	self.type = inputType || -3; // any negatives are error values
	
	// this returned is in pixels
	self.getPos = function(){
		// make a new point to avoid accidentally changing the position of the Sprite
		var result = new Point(0, 0);
		
		if(self.physBody != null){
			result.x = self.physBody.GetPosition().x * PHYSICS_SCALE;
			result.y = self.physBody.GetPosition().y * PHYSICS_SCALE;
		}
		return result;
	}
	return self;
}

function EnemySprite(inputHp){
	var self = new Sprite(ENEMY_SPRITE_ID, ENEMY_ID);
	
	// default is 1 hp if not passed
	self.hp = inputHp || 1;
	
	// point value is based on the amount of hp it has as it is harder to kill then
	self.pointValue = inputHp || 1;
	
	// if it returns false, it means the enemy is no longer alive
	self.damage = function(){
		return (--self.hp > 0);
	}
	
	return self;
}

function WallSprite(inputRect){
	var self = new Sprite(PLAYER_WALL_SPRITE_ID, WALL_ID);
	
	// only care about the wall width, as the length (or height) is a constant
	self.width = inputRect.width;
	self.rotation = inputRect.rotation;
	
	// keeps track of the current age of the wall
	self.age = WALL_AGE;
	
	// returns true if the lifeSpan is still valid
	self.updateAge = function(){
		return (--self.age > 0);
	}
	
	return self;
}

function BulletSprite(inputType, inputOwner){
	var self = new Sprite(inputType, inputOwner);
	self.hitObject = null;
	
	self.comboCounter = 0;
	
	// oddly sometimes in some cases (depending on how crazy the player makes walls), red bullets can get sandwiched between two walls
	// this causes the speed to drop very low....use this to fix the speed
	self.fixSpeed = function(){
		if(self.physBody.GetLinearVelocity().Length() < BULLET_SPEED){
			var vector = self.physBody.GetLinearVelocity();
			vector.Normalize();
			vector.Multiply(BULLET_SPEED);
		}
	}
	return self;
}

// this is a tick counter, used to determine when things should be spawned, incremented, etc
// this basically holds a counter and a 'limit' value and will return true or false when that limit is set
// the actual counting is driven by the game update loop
function TickCounter(inputLimit){
	var self = {};
	
	self.current = 0;
	self.limit = inputLimit || UPDATE_RATE; // starts off 1 per second
	
	self.update = function(){	++self.current;		}
	self.reset = function(){	self.current = 0;	}
	
	// this will check if the time is right to do something and if it is, also reset itself automatically
	self.checkAndReset = function(){
		if(self.current >= self.limit){
			self.reset();
			return true;
		} else {
			return false;
		}
	}
	return self;
}

// this is used to keep track of:
// - how many enemies are allowed at a time
// - the max amount of hp an enemy can have
// - the player's score/enemies killed
// - bullet spawn frequency
// - enemy spawn frequency
function GameState(){
	var self = {};
	
	self.playerScore = 0;
	self.playerCombo = 0
	self.playerHp = PLAYER_STARTING_HP;
	self.tickCount = 0; // currently not used, but can be used to keep track of running game time
	self.playerKillCount = 0;
	
	self.bulletTickCounter = new TickCounter(UPDATE_RATE); // starts off with a trigger rate of 1 per second
	self.enemyTickCounter = new TickCounter(UPDATE_RATE); // starts off with a trigger rate of 1 per second
	
	self.enemyCountCounter = new TickCounter(ENEMY_COUNT_INCREASE_RATE);
	self.enemyToughnessCounter = new TickCounter(ENEMY_HP_INCREASE_RATE);
	
	// starts at 5
	self.maxEnemies = STARTING_ENEMY_LIMIT;
	self.maxEnemyHp = STARTING_ENEMY_HP;
	
	self.timeToSpawnBullet = function(){	return self.bulletTickCounter.checkAndReset();	}
	self.timeToSpawnEnemy = function(){		return self.enemyTickCounter.checkAndReset();	}
		
	self.update = function(){
		self.bulletTickCounter.update();
		self.enemyTickCounter.update();
		self.tickCount++;
	}
	
	self.addPlayerKill = function(){
		self.playerKillCount++;
		
		// once the player has killed enough enemies, make the game a bit harder
		// increase the max amount of enemies on screen at any time
		self.enemyCountCounter.update();
		if(self.enemyCountCounter.checkAndReset() == true){
			self.maxEnemies++;
		}
		
		// increase the amount of hp an enemy can have as well as increase their shooting rate
		self.enemyToughnessCounter.update();
		if(self.enemyToughnessCounter.checkAndReset() == true){
			
			// increase the enemy's max hp...also make the enemies shoot a bit faster
			if(self.bulletTickCounter.limit > MAX_ENEMY_SHOOTING_SPEED){
				self.bulletTickCounter.limit -= ENEMY_SHOOT_INCREASE_RATE;
			}
			
			if(self.maxEnemyHp < ABSOLUTE_MAX_ENEMY_HP){
				self.maxEnemyHp++;
			}
		}
	} // end of addPlayerKill() method
	
	self.addEnemyHit = function(){		self.playerHp--;	}
	self.getPlayerHp = function(){		return self.playerHp;	}
	
	// returns a random number that goes from 1 (the min enemy hp) to maxEnemyHp
	self.getRandomEnemyHp = function(){
		return 1 + Math.floor(self.maxEnemyHp * Math.random());
	}
	
	self.addScore = function(inputValue){
		self.playerScore += inputValue;
	}
	
	self.updateCombo = function(inputValue){
		self.playerCombo = Math.max(inputValue, self.playerCombo);
	}
	
	self.getPlayerScore = function(){		return self.playerScore;		}
	self.getPlayerCombo = function(){		return self.playerCombo;		}
	self.getPlayerKillCount = function(){	return self.playerKillCount;	}
	
	return self;
	
} // end of GameState{} class


function PoPSprite(inputX, inputY, isBullet){
	// AnimatedSprite/pop sprites have no owner, nor are they needed...they are purely for display and have no physics
	var self = new Sprite(POP_SPRITE_ID, -1);
	self.currentFrame = -1; // this gets updated to 0 in the first game loop before it is rendered
	self.totalFrames = 7; // 8 graphic frames for this
	
	// need to keep track of position explicitly, since not using a physics body
	self.pos = new Point(inputX, inputY);
	self.getPos = function(){	return self.pos;	} // override
	
	self.update = function(){
		
		// sprite frames do not loop
		if(self.currentFrame >= self.totalFrames){
			self.donePlaying = true;
			self.currentFrame = 7; // set this to the last frame
		} else {
			self.currentFrame++;
		}
	}
	self.donePlaying = false;
	
	// keep track of this is a bubble pop for a bubble bullet or not
	self.isBullet = isBullet;
	
	return self;
}