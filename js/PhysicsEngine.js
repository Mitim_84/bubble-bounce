// this is the PhysicsEngine{} class
// It interfaces between the game logic and box2d. Core game logic shouldn't have to call box2d directly.
// It handles the creation of bubble bullets, bubble enemies, and wall physics bodies.

// box2D shortcuts
var B2Vec2 = Box2D.Common.Math.b2Vec2;
var B2World = Box2D.Dynamics.b2World;
var B2BodyDef = Box2D.Dynamics.b2BodyDef;
var B2Body = Box2D.Dynamics.b2Body;
var B2FixtureDef = Box2D.Dynamics.b2FixtureDef;
var B2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape;
var B2CircleShape = Box2D.Collision.Shapes.b2CircleShape;

// not used...doesn't seem to do anything in tests
//var B2DestructionListener = Box2D.Dynamics.b2DestructionListener;

// "b2FilterData" is an alternative to using a b2ContactListener...but it seemed more confusing for objects that change ids
//var B2FilterData = Box2D.Dynamics.b2FilterData;
var B2ContactListener = Box2D.Dynamics.b2ContactListener;


function PhysicsEngine(){
	
	var allowSleep = false;
	
	var self = {};
	
	// no gravity and no sleeping allowed
	self.world = new B2World(new B2Vec2(0, 0), allowSleep);
	
	self.addPlayer = function(spriteData, inputPosition){
		
		var playerDef = new B2BodyDef();
		playerDef.type = B2Body.b2_staticBody;
		playerDef.allowSleep = allowSleep;
		playerDef.position = new B2Vec2(inputPosition.x / PHYSICS_SCALE, inputPosition.y / PHYSICS_SCALE);
		playerDef.userData = spriteData;
		
		var playerFixture = new B2FixtureDef();
		// divided by 2 as the ENEMY_SIZE is a width and B2CircleShape takes in a radius
		playerFixture.shape = new B2CircleShape(PLAYER_SIZE / 2 / PHYSICS_SCALE);
		
		var playerBody = self.world.CreateBody(playerDef);
		playerBody.CreateFixture(playerFixture);
		
		return playerBody;
	}
	
	// just make basic for now
	// inputDirection and inputPositon should be B2Vec2 objects or Point objects (need x and y properties)
	// all values are in pixels
	self.addBullet = function(spriteData, inputPosition, inputVelocity){
		
		var bulletDef = new B2BodyDef();
		bulletDef.type = B2Body.b2_dynamicBody;
		bulletDef.allowSleep = allowSleep;
		bulletDef.position = new B2Vec2(inputPosition.x / PHYSICS_SCALE, inputPosition.y / PHYSICS_SCALE);
		
		bulletDef.userData = spriteData;
		
		bulletDef.linearVelocity = new B2Vec2(inputVelocity.x, inputVelocity.y);
		
		var bulletFixture = new B2FixtureDef();
		// divided by 2 as the BULLET_SIZE is a width and B2CircleShape takes in a radius
		bulletFixture.shape = new B2CircleShape(BULLET_SIZE / 2 / PHYSICS_SCALE);
		bulletFixture.density = 100; // seems like you cannot set mass overall? density is kg/m^2
		
		var bulletBody = self.world.CreateBody(bulletDef);
		bulletBody.CreateFixture(bulletFixture);
		
		return bulletBody;
	}
	
	// the enemy objects are static bodies
	// inputPositon should be B2Vec2 objects or Point objects (need x and y properties)
	// values are in pixels
	self.addEnemy = function(spriteData, inputPosition){
		
		var enemyDef = new B2BodyDef();
		enemyDef.type = B2Body.b2_staticBody;
		enemyDef.allowSleep = allowSleep;
		enemyDef.position = new B2Vec2(inputPosition.x / PHYSICS_SCALE, inputPosition.y / PHYSICS_SCALE);
		enemyDef.userData = spriteData;
		
		var enemyFixture = new B2FixtureDef();
		// divided by 2 as the ENEMY_SIZE is a width and B2CircleShape takes in a radius
		enemyFixture.shape = new B2CircleShape(ENEMY_SIZE / 2 / PHYSICS_SCALE);
		
		// enemy bubbles make things bounce off them 100%
		enemyFixture.restitution = 1;
		
		var enemyBody = self.world.CreateBody(enemyDef);
		enemyBody.CreateFixture(enemyFixture);
		
		return enemyBody;
	}
	
	// inputRectangle should be a Rectangle object measured in pixel coords and rotation in radians
	self.addWall = function(spriteData, inputRectangle){
		var wallDef = new B2BodyDef();
		wallDef.type = B2Body.b2_staticBody;
		wallDef.allowSleep = allowSleep;
		wallDef.angle = inputRectangle.rotation;
		wallDef.position = new B2Vec2(inputRectangle.x / PHYSICS_SCALE, inputRectangle.y / PHYSICS_SCALE);
		wallDef.userData = spriteData;
		
		var wallFixture = new B2FixtureDef();
		wallFixture.shape = new B2PolygonShape();
		wallFixture.shape.SetAsBox(inputRectangle.width / PHYSICS_SCALE / 2, inputRectangle.height / PHYSICS_SCALE / 2);
		
		// walls make things bounce off it 100%
		wallFixture.restitution = 1;
		
		var wallBody = self.world.CreateBody(wallDef);
		wallBody.CreateFixture(wallFixture);
		
		return wallBody;
	}
	
	// left in for debugging
	self.setDebugDraw = function(inputGraphics){
		var debugDraw = new Box2D.Dynamics.b2DebugDraw();
		debugDraw.SetSprite(inputGraphics);
		debugDraw.SetDrawScale(PHYSICS_SCALE); // this is a scale for pixels per meter
		debugDraw.SetFillAlpha(0.5);
		debugDraw.SetLineThickness(1.0);
		debugDraw.SetFlags(Box2D.Dynamics.b2DebugDraw.e_shapeBit);
		self.world.SetDebugDraw(debugDraw);
	}
	
	self.destroyBody = function(inputBody){
		self.world.DestroyBody(inputBody);
	}
	
	self.step = function(){
		// 16.666 ms out of 1000 ms per second => 16.666 / 1000 = 0.0166667 = 1/60 seconds each step
		self.world.Step(PHYSICS_UPDATE_RATE, 10, 10);
	}
	
	self.drawDebug = function(){
		self.world.DrawDebugData();
	}
	
	self.setContactListener = function(contactListener){
		self.world.SetContactListener(contactListener);
	}
	
	// not used...doesn't seem to do anything in tests
	/*self.destroyWorld = function(destructionListener){
		self.world.SetDestructionListener(destructionListener);
	}*/
	
	return self;
	
} // end of PhysicsEngine();