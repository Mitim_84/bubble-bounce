// this contains various classes dealing with geometry

// Point class
function Point(inputX, inputY){
	var self = {};
	
	self.__proto__ = this;
	
	self.x = inputX || 0;
	self.y = inputY || 0;
	
	self.toString = function(){
		return "("+ self.x +", "+ self.y +")";
	}
	return self;
}

// note that this is a bare bones rectangle class: the points are not really rotated by the angle as 
// it wasn't needed for the physics engine (it takes seems to take in an AABB then rotates it) 
function Rectangle(inputX, inputY, inputWidth, inputHeight, inputAngle){
	var self = new Point(inputX, inputY);
	
	self.__proto__ = this;
	
	self.width = inputWidth || 0;
	self.height = inputHeight || 0;
	
	// defaults to 0
	self.rotation = inputAngle || 0;
	
	self.left = self.x - (self.width / 2);
	self.right = self.x + (self.width / 2);
	
	self.top = self.y - (self.height / 2);
	self.bottom = self.y + (self.height / 2);
	
	self.toString = function(){
		return "Rectangle (width="+ self.width +" height="+ self.height +") @ ("+ self.x +", "+ self.y +")";
	}
	return self;
}


function Triangle(pointA, pointB, pointC){
	var self = new Point();
	
	self.__proto__ = this;
	
	self.points = new Array(3);
	
	self.points[0] = pointA || (new Point());
	self.points[1] = pointB || (new Point());
	self.points[2] = pointC || (new Point());
	
	// center of triangle
	self.x = (pointA.x + pointB.x + pointC.x) / self.points.length;
	self.y = (pointA.y + pointB.y + pointC.y) / self.points.length;
	
	// from: http://labs.grupow.com/blog/2009/05/05/random-points-inside-a-triangle
	self.getRandomPoint = function(){
		//P = aA + bB + cC
		//@see http://www.cgafaq.info/wiki/Random_Point_In_Triangle
		var a = Math.random();
		var b = Math.random();
		
		if(a + b > 1){
			a = 1-a;
			b = 1-b;
		}
		
		var c = 1-a-b;
		
		var rndX = (a * self.points[0].x) + (b * self.points[1].x) + (c * self.points[2].x);
		var rndY = (a * self.points[0].y) + (b * self.points[1].y) + (c * self.points[2].y);
		return new Point(rndX,rndY);
	}
	
	return self;
}